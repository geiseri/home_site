# README #

A template for use with Nextcloud's cms_pico app.  This theme provides a Wiki, Blog and Landing page.

## Page Types ##

### Blog ###

This page type will render content with a sidebar that has the index of all other blog posts sorted by date.
All pages in the `/blogs` folder will be included in the list.

```yaml
---
Template: blog
Date: 10/10/2017
Author: Ian Geiser
Title: First blog post
Description: This is a short blog post
---
```

### Wiki ###

This page type will render content with a sidebar with all of the pages sorted by title.  Any page
that has a template of `wiki` will be included in this list.

```yaml
---
Template: wiki
Author: Ian Geiser
Title: About wikis
---
```
